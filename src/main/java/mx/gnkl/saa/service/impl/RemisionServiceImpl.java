/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.saa.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;
import mx.gnkl.saa.dao.GenericDao;
import mx.gnkl.saa.service.RemisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jmejia
 */
@Service
public class RemisionServiceImpl implements RemisionService{

    final private String unidadesCamion = "7806B,S001";
    final private String listaMedControlado = "0107,0132.01,0202,0221,0226,0242,0243,1544,2097,2098,2099,2100,2102,2107,2108,2499,2500,2601,2602,2612,2613,2614,2619,2654,2657,3204,3215,3241,3241.01,3247,3253,3255,3259,3259.01,3302,3305,4026,4054,4057,4060,4470.01,4471.01,4472,4472.01,4481,4481.01,5351,5476,5478,5483,5484";
    
    @Autowired
    private GenericDao genericDao;    
    

    private void updateClaProUniStatus0(Map<String, String[]> reqMap, String unidades) throws NullPointerException, SQLException {
        int Existencia;
        ResultSet rset = genericDao.getResultSetGenericData("SELECT U.F_ClaPro, F_ClaUni FROM tb_unireq U INNER JOIN tb_medica M ON U.F_ClaPro=M.F_ClaPro WHERE F_ClaUni in( " + unidades + ") and  F_Status=0 and  F_Solicitado != 0 AND M.F_StsPro='A'");
        while (rset.next()) {

            String ClaPro = rset.getString("F_ClaPro");
            String[] resultParams = reqMap.get(rset.getString("F_ClaUni") + "_" + ClaPro.trim());
            String F_NCant = (resultParams!=null && resultParams.length>0)?resultParams[0]:null;
            if(F_NCant==null){
                F_NCant="0";
            }
            genericDao.updateGenericDataNoTx("UPDATE tb_unireq SET F_PiezasReq = '" + F_NCant + "' WHERE F_ClaPro = '" + rset.getString("F_ClaPro") + "' AND F_ClaUni = '" + rset.getString("F_ClaUni") + "' AND F_Status='0'");
        }
    }
    
    private Integer getGlobalFolioRemision()throws SQLException{
        String query="SELECT F_IndGlobal FROM tb_indice";
        return (Integer) genericDao.getGenericData(query, Integer.class);
    }
    
    private void processExistenciasGtPiezas(String claUni, String clave, int piezas, int folioRemision, int F_Solicitado, String FechaE, String nombre)throws SQLException{
        String queryUbicaciones = "";
        int F_IdLote, F_ExiLot, CanSur, F_FolLot, Existencia = 0, Contar = 0, DifExi, ExiMov = 0, ExiApart = 0;
        String Clave, Ubicacion="";
        
        if (unidadesCamion.contains(claUni)) {
            //si la unidad esta dentro del string de unidades camion toma las otras ubicaciones
            queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica NOT LIKE '%pick%' AND F_Ubica NOT LIKE '%PICK%' AND F_Ubica NOT LIKE '%CS%' AND  F_Ubica NOT LIKE '%CT%' AND F_Ubica NOT LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");                                    
        }else{
            queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");                                
        }


        ResultSet Ubica = genericDao.getResultSetGenericData(queryUbicaciones);
        while (Ubica.next()) {
            F_IdLote = Ubica.getInt(1);
            F_ExiLot = Ubica.getInt(2);
            F_FolLot = Ubica.getInt(3);
            Ubicacion = Ubica.getString(6);
            
            Integer resultExiApart=(Integer) genericDao.getGenericData("SELECT SUM(F.F_Cant) FROM tb_facttemp F INNER JOIN tb_lote L ON F.F_IdLot=L.F_IdLote WHERE L.F_ClaPro='" + clave + "' AND F.F_IdLot='" + F_IdLote + "' AND F.F_StsFact<5", Integer.class);
            ExiApart = resultExiApart!=null?resultExiApart:0;

            //Codigo comentado
            F_ExiLot = F_ExiLot - ExiApart;

            //existencia mayor a lo solicitado
            if ((F_ExiLot >= piezas) && (piezas > 0)) {

                //existe
                CanSur = piezas;

                genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
                //con.insertar("insert into tb_factura values(0,'" + FolioFactura + "','" + ClaUni + "','A',curdate(),'" + Clave + "','" + CanSur + "','" + CanSur + "','" + Costo + "','" + IVAPro + "','" + MontoIva + "','" + F_FolLot + "','" + FechaE + "',curtime(),'" + sesion.getAttribute("nombre") + "','" + Ubicacion + "','','" + existencia + "')");

                piezas = 0;
            } else if ((piezas > 0) && (F_ExiLot > 0)) {
                CanSur = F_ExiLot;//la cantidad a asurtir es el valor de la existencia

                if (Existencia >= piezas) {
                    F_Solicitado = CanSur;
                }

                genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
                piezas = piezas - CanSur;
                F_Solicitado = piezas;
            }
        }
    }
    
    private void processExistenciaGt0(int piezas, String claUni, String clave, int F_Solicitado, int folioRemision, String FechaE, String nombre) throws SQLException{
        ResultSet Ubica;
        int F_IdLote, F_ExiLot, CanSur, F_FolLot, Existencia = 0, Contar = 0, DifExi, ExiMov = 0, ExiApart = 0;
        String Clave, Ubicacion="";
        
        DifExi = piezas;
        int x = 1;
        String queryUbicaciones = "";
        if (unidadesCamion.contains(claUni)) {
            //si la unidad esta dentro del string de unidades camion toma las otras ubicaciones
            queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica NOT LIKE '%pick%' AND F_Ubica NOT LIKE '%PICK%' AND F_Ubica NOT LIKE '%CS%' AND  F_Ubica NOT LIKE '%CT%' AND F_Ubica NOT LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");
        }else{
            queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");
        }

        Ubica = genericDao.getResultSetGenericData(queryUbicaciones);
        /*                                        Ubica = con.consulta("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                    + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + Clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ")  AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");*/
        if (Ubica.next()) {
            F_IdLote = Ubica.getInt(1);
            F_ExiLot = Ubica.getInt(2);
            F_FolLot = Ubica.getInt(3);
            Ubicacion = Ubica.getString(6);

            Integer existApart = (Integer) genericDao.getGenericData("SELECT SUM(F.F_Cant) FROM tb_facttemp F INNER JOIN tb_lote L ON F.F_IdLot=L.F_IdLote WHERE L.F_ClaPro='" + clave + "' AND F.F_IdLot='" + F_IdLote + "' AND F.F_StsFact<5",Integer.class);    
            ExiApart = existApart!=null?existApart:0;

            F_ExiLot = F_ExiLot - ExiApart;

            CanSur = F_ExiLot;

            if (x == Contar) {
                genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
            } else if (CanSur > 0) {
                F_Solicitado = CanSur;
                genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
            }
            /*else {
                 con.insertar("INSERT INTO tb_facttemp VALUES ('"+FolioFactura+"','"+ClaUni+"','"+F_IdLote+"','"+CanSur+"','"+FechaE+"','3',0,'"+sesion.getAttribute("nombre")+"','"+F_Solicitado+"','0')");
                 }*/
        } else {

            if (unidadesCamion.contains(claUni)) {
                //si la unidad esta dentro del string de unidades camion toma las otras ubicaciones
                queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica NOT LIKE '%pick%' AND F_Ubica NOT LIKE '%PICK%' AND F_Ubica NOT LIKE '%CS%' AND  F_Ubica NOT LIKE '%CT%' AND F_Ubica NOT LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");
            }else{
                queryUbicaciones = ("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                    + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ") AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");                                            
            }

            Ubica = genericDao.getResultSetGenericData(queryUbicaciones);
            /*Ubica = con.consulta("SELECT L.F_IdLote,L.F_ExiLot,L.F_FolLot,M.F_TipMed,M.F_Costo,L.F_Ubica FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                        + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + Clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ")  AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY L.F_FecCad,L.F_ClaLot ASC ");*/

            while (Ubica.next()) {
                F_IdLote = Ubica.getInt(1);
                F_ExiLot = Ubica.getInt(2);
                F_FolLot = Ubica.getInt(3);
                Ubicacion = Ubica.getString(6);


                ExiApart = (Integer) genericDao.getGenericData("SELECT SUM(F.F_Cant) FROM tb_facttemp F INNER JOIN tb_lote L ON F.F_IdLot=L.F_IdLote WHERE L.F_ClaPro='" + clave + "' AND F.F_IdLot='" + F_IdLote + "' AND F.F_StsFact<5", Integer.class);
                F_ExiLot = F_ExiLot - ExiApart;

                CanSur = F_ExiLot;

                if (x == Contar) {
                    genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
                } else {
                    if (CanSur > 0) {
                        F_Solicitado = CanSur;
                        genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLote + "','" + CanSur + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
                        piezas = piezas - CanSur;
                        //TODO este valor F_Solicitado para que se ocupa? sirve para algo en el proceso anterior?
                        F_Solicitado = piezas;
                    }
                    /*else {
                     con.insertar("INSERT INTO tb_facttemp VALUES ('"+FolioFactura+"','"+ClaUni+"','"+F_IdLote+"','"+CanSur+"','"+FechaE+"','3',0,'"+sesion.getAttribute("nombre")+"','"+F_Solicitado+"','0')");
                     }*/

                    x = x + 1;
                    DifExi = DifExi - CanSur;
                }
            }
        }
    }
    
    public void processFolioLote(String clave, int folioRemision, String claUni, int piezas, String FechaE, String nombre, int F_Solicitado) throws SQLException{
        int FolioL = 0, IndiceLote, F_IdLoteL = 0;
        ResultSet FolLote = genericDao.getResultSetGenericData("SELECT F_FolLot,F_Ubica,F_Costo,F_IdLote FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro WHERE L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ")  AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') AND (F_Ubica LIKE '%pick%' OR F_Ubica LIKE '%CS%' OR F_Ubica LIKE '%CT%' OR F_Ubica LIKE '%CUARTO%') ORDER BY F_FolLot DESC");
        if (FolLote.next()) {
            FolioL = FolLote.getInt(1);
            F_IdLoteL = FolLote.getInt(4);
        } else {
            FolLote = genericDao.getResultSetGenericData("SELECT F_FolLot,F_Ubica,F_Costo,F_IdLote FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro WHERE L.F_ClaPro='" + clave + "' AND DATEDIFF(DATE(L.F_FecCad),now()) > 60 AND L.F_ClaPro NOT IN (" + listaMedControlado + ")  AND F_Ubica NOT IN ('REJA_DEVOL','PROX_CADUCA','MODULA','CONTROLADOS','NUEVA') ORDER BY F_FolLot DESC");
            while (FolLote.next()) {
                FolioL = FolLote.getInt(1);
                F_IdLoteL = FolLote.getInt(4);
            }
        }
        if (FolioL == 0) {
            ResultSet IndLote = genericDao.getResultSetGenericData("SELECT F_IndLote FROM tb_indice");
            if (IndLote.next()) {
                FolioL = IndLote.getInt(1);
            }
            IndiceLote = FolioL + 1;
            genericDao.updateGenericDataNoTx("update tb_indice set F_IndLote='" + IndiceLote + "'");
            genericDao.updateGenericDataNoTx("INSERT INTO tb_lote VALUES(0,'" + clave + "','X','2015-01-01','0','" + FolioL + "','1','NUEVA','2013-01-01','111','14','2','1','131')");

            ResultSet IndiceL = genericDao.getResultSetGenericData("SELECT F_IdLote FROM tb_lote WHERE F_FolLot='" + FolioL + "'");
            if (IndiceL.next()) {
                F_IdLoteL = IndiceL.getInt(1);
            }

            genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLoteL + "','" + piezas + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
        } else {
            genericDao.updateGenericDataNoTx("INSERT INTO tb_facttemp VALUES ('" + folioRemision + "','" + claUni + "','" + F_IdLoteL + "','" + piezas + "','" + FechaE + "','3',0,'" + nombre + "','" + F_Solicitado + "','0')");
        }    
    }    
    
    @Transactional
    @Override
    public void generarRemision(Map<String, String[]> reqMap, String nombreUsuario) throws SQLException, NullPointerException, ParseException, Exception {
        String[] chkUniFact = reqMap.get("chkUniFact");//unidad
        int ban1 = 0;
        ban1 = 1;
        String ClaUni = "";
        String FechaE;
        String Clave, Ubicacion;
        int piezas, folioRemision = 0, FolFact;
        String[] claveUnidadChecar = chkUniFact;
        String Unidades = "";
        
        for (int i = 0; i < claveUnidadChecar.length; i++) {
            if (i == (claveUnidadChecar.length - 1)) {
                Unidades = Unidades + "'" + claveUnidadChecar[i] + "'";
            } else {
                Unidades = Unidades + "'" + claveUnidadChecar[i] + "',";
            }
        }

        updateClaProUniStatus0(reqMap, Unidades);

        ResultSet rset = genericDao.getResultSetGenericData("select f.F_ClaUni from tb_fecharuta f, tb_uniatn u where f.F_ClaUni = u.F_ClaCli and  f.F_ClaUni in (" + Unidades + ") group by f.F_ClaUni");
        while (rset.next()) {

            folioRemision = getGlobalFolioRemision();

            FolFact = folioRemision + 1;
            genericDao.updateGenericDataNoTx("update tb_indice set F_IndGlobal='" + FolFact + "'");
            ClaUni = rset.getString("F_ClaUni");
            //FechaE = request.getParameter("F_FecEnt");
            FechaE = reqMap.get("F_FecEnt")[0];

            ResultSet rset_cantidad = genericDao.getResultSetGenericData("SELECT U.F_ClaPro,SUM(F_CajasReq) as cajas, SUM(F_PiezasReq) as piezas, F_IdReq,SUM(F_Solicitado) as F_Solicitado FROM tb_unireq U INNER JOIN tb_medica M ON U.F_ClaPro=M.F_ClaPro WHERE F_ClaUni='" + ClaUni + "' and F_Status='0' and F_Solicitado!=0 AND M.F_StsPro='A' GROUP BY F_ClaPro");
            while (rset_cantidad.next()) {
                ResultSet Ubica;
                int F_IdLote, F_ExiLot, CanSur, F_FolLot, Existencia = 0, Contar = 0, DifExi, ExiMov = 0, ExiApart = 0;
                Integer existApart = 0;
                Integer existMov = 0;
                Clave = rset_cantidad.getString("F_ClaPro");
                int piezasReq = Integer.parseInt(rset_cantidad.getString("piezas"));
                int F_Solicitado = Integer.parseInt(rset_cantidad.getString("F_Solicitado"));

                piezas = piezasReq;

                existApart = (Integer) genericDao.getGenericData("SELECT SUM(F.F_Cant) FROM tb_facttemp F INNER JOIN tb_lote L ON F.F_IdLot=L.F_IdLote AND L.F_ClaLot<>'X' WHERE L.F_ClaPro='" + Clave + "' AND F.F_StsFact<5;", Integer.class);
                existMov = (Integer) genericDao.getGenericData("select sum(F_CantMov*F_SigMov) from tb_movinv where F_ProMov='" + Clave + "'", Integer.class);

                ExiApart = existApart != null ? existApart : 0;
                ExiMov = existMov != null ? existMov : 0;

                ResultSet ExiLote = genericDao.getResultSetGenericData("SELECT SUM(L.F_ExiLot) AS F_ExiLot,COUNT(F_Ubica) AS Contar FROM tb_lote L INNER JOIN tb_medica M ON L.F_ClaPro=M.F_ClaPro "
                        + "WHERE L.F_ExiLot>0 AND M.F_Catipo='1' AND M.F_StsPro='A' AND L.F_ClaPro='" + Clave + "'");
                while (ExiLote.next()) {
                    Existencia = ExiLote.getInt(1);
                    Contar = ExiLote.getInt(2);
                }

                Existencia = Existencia - ExiApart;
                if (Existencia > 0) {
                    if (Existencia >= piezas) {
                        processExistenciasGtPiezas(ClaUni, Clave, piezas, folioRemision, F_Solicitado, FechaE, nombreUsuario);
                    } else if (Existencia > 0) {
                        processExistenciaGt0(piezas, ClaUni, Clave, F_Solicitado, folioRemision, FechaE, nombreUsuario);
                    } else {
                        processFolioLote(Clave, folioRemision, ClaUni, piezas, FechaE, nombreUsuario, F_Solicitado);
                    }
                } else {
                    processFolioLote(Clave, folioRemision, ClaUni, piezas, FechaE, nombreUsuario, F_Solicitado);
                }
                genericDao.updateGenericDataNoTx("update tb_unireq set F_Status='2' where F_IdReq='" + rset_cantidad.getString("F_IdReq") + "'");
            }
            genericDao.updateGenericDataNoTx("update tb_unireq set F_Status='2' where F_ClaUni='" + ClaUni + "' and F_Status='0' ");
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.saa.service;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface RemisionService {
    
    public void generarRemision(Map<String, String[]> reqMap, String nombre) throws SQLException, NullPointerException, ParseException, Exception;
    
}

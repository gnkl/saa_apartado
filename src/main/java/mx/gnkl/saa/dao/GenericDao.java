/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.saa.dao;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author jmejia
 * @param <T>
 */
public interface GenericDao<T extends Serializable> {
    public T getGenericData(String query, T bean) throws NullPointerException, SQLException;
    public T getGenericData(String query, Class<T> clase) throws NullPointerException, SQLException;
    public List<T> getListGenericData(String query, T bean) throws NullPointerException, SQLException;
    public ResultSet getResultSetGenericData(String query) throws NullPointerException, SQLException;
    public int updateGenericData(String query) throws NullPointerException, SQLException;
    public int updateGenericDataNoTx(String query) throws NullPointerException, SQLException;
    
}

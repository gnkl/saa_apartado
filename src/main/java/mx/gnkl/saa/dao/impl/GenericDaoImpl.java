/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.saa.dao.impl;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.gnkl.saa.dao.GenericDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.ResultSetWrappingSqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jmejia
 * @param <T>
 */
@Repository("genericDao")
public class GenericDaoImpl<T extends Serializable> implements GenericDao<T>{

    @Autowired
    private JdbcTemplate jdbcTemplateLerma;

    @Override
    public T getGenericData(String query, T bean) throws NullPointerException, SQLException{
        try {
            System.out.println(query);
            return (T) this.jdbcTemplateLerma.queryForObject(query, new BeanPropertyRowMapper((Class) bean));
        }catch(Exception ex){
            Logger.getLogger(GenericDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public T getGenericData(String query, Class<T> clase) throws NullPointerException, SQLException{
        try {
            System.out.println(query);
            return (T) this.jdbcTemplateLerma.queryForObject(query, clase);
        }catch(Exception ex){
            Logger.getLogger(GenericDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    
    @Override
    public List<T> getListGenericData(String query, T bean) throws NullPointerException, SQLException{
        System.out.println(query);
        return this.jdbcTemplateLerma.query(query, new BeanPropertyRowMapper((Class) bean));
    }

    @Override
    public ResultSet getResultSetGenericData(String query) throws NullPointerException, SQLException{
        try {
            System.out.println(query);
            SqlRowSet results =this.jdbcTemplateLerma.queryForRowSet(query);
            return ((ResultSetWrappingSqlRowSet) results).getResultSet();
        }catch(Exception ex ){
            Logger.getLogger(GenericDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }      
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateGenericData(String query) throws NullPointerException, SQLException {
        System.out.println(query);
        return this.jdbcTemplateLerma.update(query);
    }

    @Override
    public int updateGenericDataNoTx(String query) throws NullPointerException, SQLException {
        System.out.println(query);
        return this.jdbcTemplateLerma.update(query);
    }    
}
